package it.unimi.di.prog2.lab8.model;

import it.unimi.di.prog2.lab8.Observer;
import org.jetbrains.annotations.NotNull;

public class TemperatureModel implements Model {
  @Override
  public void notifyObservers() {

  }

  @Override
  public void addObserver(@NotNull Observer<Double> obs) {

  }

  @Override
  public @NotNull Double getState() {
    return null;
  }

  @Override
  public double getTemp() {
    return 0;
  }

  @Override
  public void setTemp(double temp) {

  }

  //TODO implementare i doveri ereditati dalla interfaccia

}
